/*
 * Copyright (C) 2018 Alexandros Theodotou
 *
 * This file is part of ZChordz
 *
 * ZChordz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZChordz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZChordz.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DistrhoPlugin.hpp"

START_NAMESPACE_DISTRHO

// -----------------------------------------------------------------------------------------------------------

/**
  Plugin that demonstrates the latency API in DPF.
 */
class ZChordz : public Plugin
{
public:
    enum Parameters
    {
        paramScale,
        paramMajor,
        paramBass,
        paramFirst,
        paramThird,
        paramFifth,
        paramSeventh,
        paramOctave,
        paramNinth,
        paramEleventh,
        paramThirteenth
    };

    enum Scales
      {
        scaleC,
        scaleCs,
        scaleD,
        scaleDs,
        scaleE,
        scaleF,
        scaleFs,
        scaleG,
        scaleGs,
        scaleA,
        scaleAs,
        scaleB
      };

    ZChordz();

protected:
   /* --------------------------------------------------------------------------------------------------------
    * Information */

   /**
      Get the plugin label.
      This label is a short restricted name consisting of only _, a-z, A-Z and 0-9 characters.
    */
    const char* getLabel() const override
    {
        return DISTRHO_PLUGIN_NAME;
    }

   /**
      Get an extensive comment/description about the plugin.
    */
    const char* getDescription() const override
    {
        return "Assign chords to keys";
    }

   /**
      Get the plugin author/maker.
    */
    const char* getMaker() const override
    {
        return DISTRHO_PLUGIN_BRAND;
    }

   /**
      Get the plugin homepage.
    */
    const char* getHomePage() const override
    {
        return "https://gitlab.com/alextee/zchordz";
    }

   /**
      Get the plugin license name (a single line of text).
      For commercial plugins this should return some short copyright information.
    */
    const char* getLicense() const override
    {
        return "GPLv3";
    }

   /**
      Get the plugin version, in hexadecimal.
    */
    uint32_t getVersion() const override
    {
        return d_version(0, 0, 1);
    }

   /**
      Get the plugin unique Id.
      This value is used by LADSPA, DSSI and VST plugin formats.
    */
    int64_t getUniqueId() const override
    {
        return d_cconst('Z', 'C', 'h', 'r');
    }

   /* --------------------------------------------------------------------------------------------------------
    * Init and Internal data */

   /**
      Initialize the parameter @a index.
      This function will be called once, shortly after the plugin is created.
    */
    void initParameter(uint32_t index, Parameter& parameter) override;

   /**
       Set the name of the program @a index.
       This function will be called once, shortly after the plugin is created.
     */
   void initProgramName(uint32_t index, String& programName);

   /**
      Get the current value of a parameter.
    */
    float getParameterValue(uint32_t index) const override;

   /**
      Change a parameter value.
    */
    void setParameterValue(uint32_t index, float value) override;

  /**
     Load a program.
   */
   void loadProgram(uint32_t index);

   /* --------------------------------------------------------------------------------------------------------
    * Audio/MIDI Processing */

   /**
      Run/process function for plugins with MIDI input.
      In this case we just pass-through all MIDI events.
    */
    void run(const float**, float**, uint32_t,
             const MidiEvent* midiEvents, uint32_t midiEventCount) override;

      void handleRawEvent (const MidiEvent * src, ///< the source event
                                MidiEvent * dest, ///< a place to store results
                              uint32_t * dest_size); ///< a place to store results size

    // -------------------------------------------------------------------------------------------------------

private:
   /**
      Meter values.
      These are the parameter outputs from the DSP side.
    */
    float scale, /* 0-11 */
          is_major, /* 0 or 1 */
          bass,
          first,
          third,
          fifth,
          seventh,
          octave,
          ninth,
          eleventh,
          thirteenth;




   /**
      Set our plugin class as non-copyable and add a leak detector just in case.
    */
    DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(ZChordz)
};

/* ------------------------------------------------------------------------------------------------------------
 * Plugin entry point, called by DPF to create a new plugin instance. */

Plugin* createPlugin()
{
    return new ZChordz();
}

// -----------------------------------------------------------------------------------------------------------

END_NAMESPACE_DISTRHO
