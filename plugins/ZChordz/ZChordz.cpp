/*
 * Copyright (C) 2018 Alexandros Theodotou
 *
 * This file is part of ZChordz
 *
 * ZChordz is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZChordz is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZChordz.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ZChordz.hpp"

START_NAMESPACE_DISTRHO

// -----------------------------------------------------------------------------------------------------------

/**
  Plugin that demonstrates the latency API in DPF.
 */
ZChordz::ZChordz()
        : Plugin(11, 12, 0),  /* paramers, programs, states */
        scale (0.0f),
        is_major (0.0f),
        bass (1.0f),
        first (1.0f),
        third (1.0f),
        fifth (1.0f),
        seventh (0.0f),
        octave (0.0f),
        ninth (0.0f),
        eleventh (0.0f),
        thirteenth (0.0f)
{
  loadProgram (0);
}

/* --------------------------------------------------------------------------------------------------------
  * Init and Internal data */

/**
  Initialize the parameter @a index.
  This function will be called once, shortly after the plugin is created.
*/
void ZChordz::initParameter(uint32_t index, Parameter& parameter)
{
   /**
      Defaults
    */
    parameter.ranges.min = 0.0f;
    parameter.ranges.max = 1.0f;
    parameter.ranges.def = 0.0f;

   /**
      Set parameter data.
    */
    switch (index)
    {
    case paramScale:
        parameter.name   = "scale";
        parameter.ranges.max = 11.9f;
        break;
    case paramMajor:
        parameter.name   = "major";
        break;
    case paramBass:
        parameter.name   = "vel_bass";
        break;
    case paramFirst:
        parameter.name   = "vel_base";
        break;
    case paramThird:
        parameter.name   = "vel_3rd";
        break;
    case paramFifth:
        parameter.name   = "vel_5th";
        break;
    case paramSeventh:
        parameter.name   = "vel_7th";
        break;
    case paramOctave:
        parameter.name   = "vel_oct";
        break;
    case paramNinth:
        parameter.name   = "vel_9th";
        break;
    case paramEleventh:
        parameter.name   = "vel_11th";
        break;
    case paramThirteenth:
        parameter.name   = "vel_13th";
        break;
    }

   /**
      Our parameter names are valid symbols except for "-".
    */
    parameter.symbol = parameter.name;
    parameter.symbol.replace('-', '_');
}

/**
   Set the name of the program @a index.
   This function will be called once, shortly after the plugin is created.
 */
void ZChordz::initProgramName(uint32_t index, String& programName)
{
   switch(index)
   {
   case scaleC:
       programName = "C";
       break;
   case scaleCs:
       programName = "C#";
       break;
   case scaleD:
       programName = "D";
       break;
   case scaleDs:
       programName = "D#";
       break;
   case scaleE:
       programName = "E";
       break;
   case scaleF:
       programName = "F";
       break;
   case scaleFs:
       programName = "F#";
       break;
   case scaleG:
       programName = "G";
       break;
   case scaleGs:
       programName = "G#";
       break;
   case scaleA:
       programName = "A";
       break;
   case scaleAs:
       programName = "A#";
       break;
   case scaleB:
       programName = "B";
       break;
   }
}

/**
  Get the current value of a parameter.
*/
float ZChordz::getParameterValue(uint32_t index) const
{
    switch (index)
    {
    case paramScale: return scale;
    case paramMajor: return is_major;
    case paramBass: return bass;
    case paramFirst: return first;
    case paramThird: return third;
    case paramFifth: return fifth;
    case paramSeventh: return seventh;
    case paramOctave: return octave;
    case paramNinth: return ninth;
    case paramEleventh: return eleventh;
    case paramThirteenth: return thirteenth;
    }

    return 0.0f;
}

/**
  Change a parameter value.
*/
void ZChordz::setParameterValue(uint32_t index, float value)
{
  switch (index)
    {
    case paramScale:
      scale = (int) value;
      loadProgram (scale);
      break;
    case paramMajor:
      is_major = value > 0.5f ? 1 : 0;
    case paramBass:
      bass = value;
      break;
    case paramFirst:
      first = value;
      break;
    case paramThird:
      third = value;
      break;
    case paramFifth:
      fifth = value;
      break;
    case paramSeventh:
      seventh = value;
      break;
    case paramOctave:
      octave = value;
      break;
    case paramNinth:
      ninth = value;
      break;
    case paramEleventh:
      eleventh = value;
      break;
    case paramThirteenth:
      thirteenth = value;
      break;
    }
}

const int major_scale[12] = { 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1 };
const int minor_scale[12] = { 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0 };

static int get_semitone_diff_from_scale_root (int nth_white_key, int major)
{
  int count = -1;
  for (int i = 0; i < 12; i++)
    {
      if (major)
        {
          if (major_scale[i] == 1)
            count++;
        }
      else
        {
          if (minor_scale[i] == 1)
            count++;
        }
      if (count == nth_white_key)
        return i;
    }
}

/**
 */
static int get_base_note (int octave, int nth_white_key, int scale, int major)
{
  return octave * 12 + scale + get_semitone_diff_from_scale_root (nth_white_key, major);
}

static int get_bass_note (int octave, int nth_white_key, int scale, int major)
{
  return get_base_note (octave, nth_white_key, scale, major) - 12;
}

static int get_third_note (int octave, int nth_white_key, int scale, int major)
{
  int keys_to_add = 0;

  if (major)
    {
      switch (nth_white_key)
        {
          /* minor / diminished */
        case 1:
        case 2:
        case 5:
        case 6:
          keys_to_add = 3;
          break;
          /* major */
        case 0:
        case 3:
        case 4:
          keys_to_add = 4;
          break;
        }
    }
  else
    {
      switch (nth_white_key)
        {
          /* minor / diminished */
        case 0:
        case 1:
        case 3:
        case 4:
          keys_to_add = 3;
          break;
          /* major */
        case 2:
        case 5:
        case 6:
          keys_to_add = 4;
          break;
        }
    }

  return get_base_note (octave, nth_white_key, scale, major) + keys_to_add;
}

static int get_fifth_note (int octave, int nth_white_key, int scale, int major)
{
  int keys_to_add = 7;

  if (major)
    {
      if (nth_white_key == 6)
        keys_to_add = 6; /* diminished */
    }
  else
    {
      if (nth_white_key == 1)
        keys_to_add = 6; /* diminished */
    }

  return get_base_note (octave, nth_white_key, scale, major) + keys_to_add;
}

static int get_seventh_note (int octave, int nth_white_key, int scale, int major)
{
  int keys_to_add = 0;

  if (major)
    {
      switch (nth_white_key)
        {
          /* diminished */
        case 6:
          keys_to_add = 9;
          break;
          /* minor */
        case 1:
        case 2:
        case 5:
          keys_to_add = 10;
          break;
          /* major */
        case 0:
        case 3:
        case 4:
          keys_to_add = 11;
          break;
        }
    }
  else
    {
      switch (nth_white_key)
        {
          /* diminished */
        case 1:
          keys_to_add = 9;
          break;
          /* minor */
        case 0:
        case 3:
        case 4:
          keys_to_add = 10;
          break;
          /* major */
        case 2:
        case 5:
        case 6:
          keys_to_add = 11;
          break;
        }
    }

  return get_base_note (octave, nth_white_key, scale, major) + keys_to_add;
}

static int get_ninth_note (int octave, int nth_white_key, int scale, int major)
{
  int keys_to_add = 1;
  int semitone_diff = get_semitone_diff_from_scale_root (nth_white_key, major);
  int base_note = get_base_note (octave, nth_white_key, scale, major);
  if (major)
    {
      do
        {
          if (major_scale[(semitone_diff + keys_to_add) % 12] == 1)
            break;
          else
            keys_to_add++;
        } while (1);
    }
  else
    {
      do
        {
          if (minor_scale[(semitone_diff + keys_to_add) % 12] == 1)
            break;
          else
            keys_to_add++;

        } while (1);
    }

  return base_note + 12 + keys_to_add;
}

static int get_eleventh_note (int octave, int nth_white_key, int scale, int major)
{
  int keys_to_add = 5;
  int semitone_diff = get_semitone_diff_from_scale_root (nth_white_key, major);
  int base_note = get_base_note (octave, nth_white_key, scale, major);
  if (major)
    {
      do
        {
          if (major_scale[(semitone_diff + keys_to_add) % 12] == 1)
            break;
          else
            keys_to_add++;
        } while (1);
    }
  else
    {
      do
        {
          if (minor_scale[(semitone_diff + keys_to_add) % 12] == 1)
            break;
          else
            keys_to_add++;

        } while (1);
    }

  return base_note + 12 + keys_to_add;
}

static int get_thirteenth_note (int octave, int nth_white_key, int scale, int major)
{
  int keys_to_add = 8;
  int semitone_diff = get_semitone_diff_from_scale_root (nth_white_key, major);
  int base_note = get_base_note (octave, nth_white_key, scale, major);
  if (major)
    {
      do
        {
          if (major_scale[(semitone_diff + keys_to_add) % 12] == 1)
            break;
          else
            keys_to_add++;
        } while (1);
    }
  else
    {
      do
        {
          if (minor_scale[(semitone_diff + keys_to_add) % 12] == 1)
            break;
          else
            keys_to_add++;

        } while (1);
    }

  return base_note + 12 + keys_to_add;
}

/**
 Load a program.
*/
void ZChordz::loadProgram(uint32_t index)
{
   scale = index; /* transpose */
}

/* --------------------------------------------------------------------------------------------------------
* Audio/MIDI Processing */

/**
 * Returns multiple midi events for each midi event, for each note in the chord
 */
void ZChordz::handleRawEvent (const MidiEvent * src, ///< the source event
                              MidiEvent * dest, ///< a place to store results
                              uint32_t * dest_size) ///< a place to store results size
{

  const uint8_t size = src->size;
  const uint8_t* const data = src->data;
  int create_event = 0;
    if (size != 3)
        return;

    switch (data[0] & 0xf0)
    {
    case 0x80:
      create_event = 1;
      break;
    case 0x90:
      create_event = 1;
        break;
    case 0xB0:
        break;
    default:
        break;
    }

    if (create_event)
        {
          (* dest_size) = 0;
    unsigned char note = data[1];
    unsigned char vel = data[2];
    int nth_white_key = -1;

      uint32_t _octave = note / 12;
      MidiEvent * midi_event;
      switch (note % 12)
        {
        case 0: /* C pressed */
          nth_white_key = 0;
          break;
        case 1: /* C# pressed TODO use black keys for modifiers (adding extra notes?)*/

          break;
        case 2: /* D pressed */
          nth_white_key = 1;

          break;
        case 3: /* D# pressed */

          break;
        case 4: /* E pressed */
          nth_white_key = 2;

          break;
        case 5: /* F pressed */
          nth_white_key = 3;

          break;
        case 6: /* F# pressed */

          break;
        case 7: /* G pressed */
          nth_white_key = 4;

          break;
        case 8: /* G# pressed */

          break;
        case 9: /* A pressed */
          nth_white_key = 5;

          break;
        case 10: /* A# pressed */

          break;
        case 11: /* B pressed */
          nth_white_key = 6;

          break;
        }

      if (nth_white_key > -1)
        {
          int min_val = 0.01f;
          if (first > min_val)
            {
              midi_event = &dest[(* dest_size)++];
              midi_event->size = size;
              midi_event->frame = src->frame;
              midi_event->data[0] = data[0];
              midi_event->data[1] = get_base_note (_octave, nth_white_key, scale, is_major);
              midi_event->data[2] = vel * first;
              //midi_event->data[3] = src->data[3];
              //midi_event->dataExt = src->dataExt;
            }

          if (bass > min_val)
            {
              midi_event = &dest[(* dest_size)++];
              midi_event->size = size;
              midi_event->frame = src->frame;
              midi_event->data[0] = data[0];
              midi_event->data[1] = get_bass_note (_octave, nth_white_key, scale, is_major);
              midi_event->data[2] = vel * bass;
              //midi_event->data[3] = src->data[3];
              //midi_event->dataExt = src->dataExt;
            }

          if (third > min_val)
            {
              midi_event = &dest[(* dest_size)++];
              midi_event->size = size;
              midi_event->frame = src->frame;
              midi_event->data[0] = data[0];
              midi_event->data[1] = get_third_note (_octave, nth_white_key, scale, is_major);
              midi_event->data[2] = vel * third;
              //midi_event->data[3] = src->data[3];
              //midi_event->dataExt = src->dataExt;
            }

          if (fifth > min_val)
            {
                midi_event = &dest[(* dest_size)++];
                midi_event->size = size;
                midi_event->frame = src->frame;
                midi_event->data[0] = data[0];
                midi_event->data[1] = get_fifth_note (_octave, nth_white_key, scale, is_major);
                midi_event->data[2] = vel * fifth;
                //midi_event->data[3] = src->data[3];
                //midi_event->dataExt = src->dataExt;
            }

          if (seventh > min_val)
            {
                midi_event = &dest[(* dest_size)++];
                midi_event->size = size;
                midi_event->frame = src->frame;
                midi_event->data[0] = data[0];
                midi_event->data[1] = get_seventh_note (_octave, nth_white_key, scale,is_major);
                midi_event->data[2] = vel * seventh;
                //midi_event->data[3] = src->data[3];
                //midi_event->dataExt = src->dataExt;
            }

          if (octave > min_val)
            {
                midi_event = &dest[(* dest_size)++];
                midi_event->size = size;
                midi_event->frame = src->frame;
                midi_event->data[0] = data[0];
                midi_event->data[1] = get_base_note (_octave, nth_white_key, scale, is_major) + 12;
                midi_event->data[2] = vel * octave;
                //midi_event->data[3] = src->data[3];
                //midi_event->dataExt = src->dataExt;
            }

          if (ninth > min_val)
            {
                midi_event = &dest[(* dest_size)++];
                midi_event->size = size;
                midi_event->frame = src->frame;
                midi_event->data[0] = data[0];
                midi_event->data[1] = get_ninth_note (_octave, nth_white_key, scale, is_major);
                midi_event->data[2] = vel * ninth;
                //midi_event->data[3] = src->data[3];
                //midi_event->dataExt = src->dataExt;
            }

          if (eleventh > min_val)
            {
                midi_event = &dest[(* dest_size)++];
                midi_event->size = size;
                midi_event->frame = src->frame;
                midi_event->data[0] = data[0];
                midi_event->data[1] = get_eleventh_note (_octave, nth_white_key, scale, is_major);
                midi_event->data[2] = vel * eleventh;
                //midi_event->data[3] = src->data[3];
                //midi_event->dataExt = src->dataExt;
            }

          if (thirteenth > min_val)
            {
                midi_event = &dest[(* dest_size)++];
                midi_event->size = size;
                midi_event->frame = src->frame;
                midi_event->data[0] = data[0];
                midi_event->data[1] = get_thirteenth_note (_octave, nth_white_key, scale, is_major);
                midi_event->data[2] = vel * thirteenth;
                //midi_event->data[3] = src->data[3];
                //midi_event->dataExt = src->dataExt;
            }
        }
        }

}

/**
  Run/process function for plugins with MIDI input.
  In this case we just pass-through all MIDI events.
*/
void ZChordz::run(const float**, float** outputs, uint32_t frames,
         const MidiEvent* midiEvents, uint32_t midiEventCount)
{
   uint32_t curEventIndex = 0;
    MidiEvent new_events[20];
    uint32_t new_size = 0;

   /* process any ready events */
    while (curEventIndex < midiEventCount)
    {
        if (midiEvents[curEventIndex].size > MidiEvent::kDataSize)
            continue;

        handleRawEvent(&midiEvents[curEventIndex], new_events, &new_size);
        curEventIndex++;
  }

    for (uint32_t i=0; i<new_size; ++i)
        writeMidiEvent(new_events[i]);
}


// -----------------------------------------------------------------------------------------------------------

END_NAMESPACE_DISTRHO
